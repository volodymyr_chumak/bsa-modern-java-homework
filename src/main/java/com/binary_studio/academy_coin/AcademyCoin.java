package com.binary_studio.academy_coin;

import java.util.stream.Stream;

public final class AcademyCoin {

	private AcademyCoin() {
	}

	public static int maxProfit(Stream<Integer> prices) {
		int profit = 0;
		int localMinimum = 0;

		int[] values = prices.mapToInt(n -> n).toArray();

		for (int i = 1; i < values.length; i++) {
			if (values[i] < values[i - 1]) {
				localMinimum = i;
			}

			if (values[i - 1] < values[i] && (i + 1 == values.length || values[i + 1] < values[i])) {
				profit += (values[i] - values[localMinimum]);
			}
		}

		return profit;
	}

}
