package com.binary_studio.dependency_detector;

public final class DependencyDetector {

	private DependencyDetector() {
	}

	public static boolean canBuild(DependencyList libraries) {
		DependencyService dependencyService = new DependencyServiceImpl();
		dependencyService.addLibraries(libraries.libraries);
		dependencyService.addDependencies(libraries.dependencies);

		return !dependencyService.hasCyclicDependencies();
	}

}
