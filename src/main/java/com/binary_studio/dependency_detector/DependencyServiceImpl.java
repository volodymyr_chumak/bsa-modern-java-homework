package com.binary_studio.dependency_detector;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DependencyServiceImpl implements DependencyService {

	private Graph graph;

	private List<Vertex> vertices;

	private Map<String, Vertex> verticesMap;

	@Override
	public void addLibraries(List<String> libraries) {
		this.vertices = libraries.stream().map(Vertex::new).collect(Collectors.toList());
		this.verticesMap = this.vertices.stream().collect(Collectors.toMap(Vertex::getLabel, v -> v));
		createGraph();
	}

	private void createGraph() {
		this.graph = new Graph(this.vertices);
	}

	@Override
	public void addDependencies(List<String[]> dependencies) {
		dependencies.forEach(
				depPair -> this.graph.addEdge(this.verticesMap.get(depPair[0]), this.verticesMap.get(depPair[1])));
	}

	@Override
	public boolean hasCyclicDependencies() {
		return this.graph.hasCycle();
	}

}
