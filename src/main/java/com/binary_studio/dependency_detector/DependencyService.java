package com.binary_studio.dependency_detector;

import java.util.List;

public interface DependencyService {

	void addLibraries(List<String> libraries);

	void addDependencies(List<String[]> dependencies);

	boolean hasCyclicDependencies();

}
