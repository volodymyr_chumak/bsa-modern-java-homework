package com.binary_studio.dependency_detector;

import java.util.ArrayList;
import java.util.List;

public class Vertex {

	private String label;

	private boolean visited;

	private boolean beingVisited;

	private List<Vertex> adjacencyList;

	public Vertex(String label) {
		this.label = label;
		this.adjacencyList = new ArrayList<>();
	}

	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public boolean isVisited() {
		return this.visited;
	}

	public void setVisited(boolean visited) {
		this.visited = visited;
	}

	public boolean isBeingVisited() {
		return this.beingVisited;
	}

	public void setBeingVisited(boolean beingVisited) {
		this.beingVisited = beingVisited;
	}

	public List<Vertex> getAdjacencyList() {
		return this.adjacencyList;
	}

	public void setAdjacencyList(List<Vertex> adjacencyList) {
		this.adjacencyList = adjacencyList;
	}

	public void addNeighbour(Vertex adjacent) {
		this.adjacencyList.add(adjacent);
	}

}
