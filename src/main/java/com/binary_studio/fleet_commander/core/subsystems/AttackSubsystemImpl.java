package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private final String name;

	private final PositiveInteger powergridRequirments;

	private final PositiveInteger capacitorConsumption;

	private final PositiveInteger optimalSpeed;

	private final PositiveInteger optimalSize;

	private final PositiveInteger baseDamage;

	public AttackSubsystemImpl(String name, PositiveInteger powergridRequirments, PositiveInteger capacitorConsumption,
			PositiveInteger optimalSpeed, PositiveInteger optimalSize, PositiveInteger baseDamage) {
		this.name = name;
		this.powergridRequirments = powergridRequirments;
		this.capacitorConsumption = capacitorConsumption;
		this.optimalSpeed = optimalSpeed;
		this.optimalSize = optimalSize;
		this.baseDamage = baseDamage;
	}

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {

		if (isNameIncorrect(name)) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		return new AttackSubsystemImpl(name, powergridRequirments, capacitorConsumption, optimalSpeed, optimalSize,
				baseDamage);
	}

	private static boolean isNameIncorrect(String name) {
		return name == null || name.trim().isEmpty();
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridRequirments;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		double sizeReductionModifier = calcSizeReductionModifier(target);
		double speedReductionModifier = calcSpeedReductionModifier(target);

		return calcDamageForTarget(sizeReductionModifier, speedReductionModifier);
	}

	private double calcSizeReductionModifier(Attackable target) {
		double sizeReductionModifier;
		double targetSize = target.getSize().value();
		double optimalSize = this.optimalSize.value();
		if (targetSize >= optimalSize) {
			sizeReductionModifier = 1;
		}
		else {
			sizeReductionModifier = targetSize / optimalSize;
		}

		return sizeReductionModifier;
	}

	private double calcSpeedReductionModifier(Attackable target) {
		double speedReductionModifier;
		double targetSpeed = target.getCurrentSpeed().value();
		double optimalSpeed = this.optimalSpeed.value();
		if (targetSpeed <= optimalSpeed) {
			speedReductionModifier = 1;
		}
		else {
			speedReductionModifier = optimalSpeed / (2 * targetSpeed);
		}

		return speedReductionModifier;
	}

	private PositiveInteger calcDamageForTarget(double sizeReductionModifier, double speedReductionModifier) {
		double damage = this.baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier);

		return PositiveInteger.of((int) Math.ceil(damage));
	}

	@Override
	public String getName() {
		return this.name;
	}

}
