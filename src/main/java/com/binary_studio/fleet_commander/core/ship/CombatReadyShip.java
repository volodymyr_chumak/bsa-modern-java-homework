package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private final String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private final PositiveInteger maxShieldHP;

	private final PositiveInteger maxHullHP;

	private PositiveInteger capacitorAmount;

	private final PositiveInteger maxCapacitorAmount;

	private final PositiveInteger capacitorRechargeRate;

	private final PositiveInteger speed;

	private final PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public CombatReadyShip(DockedShip dockedShip) {
		this.name = dockedShip.getName();
		this.shieldHP = dockedShip.getShieldHP();
		this.hullHP = dockedShip.getHullHP();
		this.capacitorAmount = dockedShip.getCapacitorAmount();
		this.capacitorRechargeRate = dockedShip.getCapacitorRechargeRate();
		this.speed = dockedShip.getSpeed();
		this.size = dockedShip.getSize();
		this.attackSubsystem = dockedShip.getAttackSubsystem();
		this.defenciveSubsystem = dockedShip.getDefenciveSubsystem();

		this.maxShieldHP = dockedShip.getShieldHP();
		this.maxHullHP = dockedShip.getHullHP();
		this.maxCapacitorAmount = dockedShip.getCapacitorAmount();
	}

	@Override
	public void endTurn() {
		int capacitorLack = this.maxCapacitorAmount.value() - this.capacitorAmount.value();
		int capacitorRechargeRate = this.capacitorRechargeRate.value();
		if (capacitorLack < capacitorRechargeRate) {
			this.capacitorAmount = this.maxCapacitorAmount;
		}
		else {
			this.capacitorAmount = PositiveInteger.of(this.capacitorAmount.value() + capacitorRechargeRate);
		}
	}

	@Override
	public void startTurn() {

	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		int requiredCapacity = this.attackSubsystem.getCapacitorConsumption().value();
		Optional<AttackAction> attackResults = Optional.empty();

		if (shipHasEnoughCapacityForAttack(requiredCapacity)) {
			attackResults = getAttackResults(target);
			this.capacitorAmount = PositiveInteger.of(this.capacitorAmount.value() - requiredCapacity);
		}

		return attackResults;
	}

	private boolean shipHasEnoughCapacityForAttack(int requiredCapacity) {
		int capacitorAmount = this.capacitorAmount.value();
		return capacitorAmount >= requiredCapacity;
	}

	private Optional<AttackAction> getAttackResults(Attackable target) {
		PositiveInteger damage = this.attackSubsystem.attack(target);
		return Optional.of(new AttackAction(damage, this, target, this.attackSubsystem));
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackResult attackResult;
		int incomingDamage = this.defenciveSubsystem.reduceDamage(attack).damage.value();

		if (this.shieldHP.value() >= incomingDamage) {
			this.shieldHP = getShieldHpAfterAttack(incomingDamage);
			attackResult = getIncomingAttackResult(attack, incomingDamage);
		}
		else {
			if (hullDestroyedAfterAttack(incomingDamage)) {
				this.hullHP = PositiveInteger.of(0);
				attackResult = new AttackResult.Destroyed();
			}
			else {
				this.hullHP = PositiveInteger.of(this.hullHP.value() - incomingDamage);
				attackResult = getIncomingAttackResult(attack, incomingDamage);
			}
		}

		return attackResult;
	}

	private PositiveInteger getShieldHpAfterAttack(int incomingDamage) {
		return PositiveInteger.of(this.shieldHP.value() - incomingDamage);
	}

	private boolean hullDestroyedAfterAttack(int incomingDamage) {
		int damageForHull = incomingDamage;
		if (this.shieldHP.value() > 0) {
			damageForHull = incomingDamage - this.shieldHP.value();
			this.shieldHP = PositiveInteger.of(0);
		}
		int hullAfterAttack = this.hullHP.value() - damageForHull;

		return hullAfterAttack < incomingDamage;
	}

	private AttackResult getIncomingAttackResult(AttackAction attack, int incomingDamage) {
		return new AttackResult.DamageRecived(attack.weapon, PositiveInteger.of(incomingDamage), this);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (shipHasEnoughCapacity()) {
			reduceCapacitorAmount();
		}
		else {
			return Optional.empty();
		}

		RegenerateAction regenerateAction = this.defenciveSubsystem.regenerate();
		PositiveInteger regeneratedHullHP = regenerateAction.hullHPRegenerated;
		PositiveInteger regeneratedShieldHP = regenerateAction.shieldHPRegenerated;

		PositiveInteger hullRegenResult = getRegenResult(this.hullHP, this.maxHullHP, regeneratedHullHP);
		PositiveInteger shieldRegenResult = getRegenResult(this.shieldHP, this.maxShieldHP, regeneratedShieldHP);
		setRegeneratedHP(shieldRegenResult, hullRegenResult);

		return Optional.of(new RegenerateAction(shieldRegenResult, hullRegenResult));
	}

	private boolean shipHasEnoughCapacity() {
		int requiredCapacity = this.defenciveSubsystem.getCapacitorConsumption().value();
		return requiredCapacity <= this.capacitorAmount.value();
	}

	private void reduceCapacitorAmount() {
		int requiredCapacity = this.defenciveSubsystem.getCapacitorConsumption().value();
		this.capacitorAmount = PositiveInteger.of(this.capacitorAmount.value() - requiredCapacity);
	}

	private PositiveInteger getRegenResult(PositiveInteger currentHP, PositiveInteger maxHP,
			PositiveInteger regenRate) {
		int regenResult = 0;
		int lackHP = maxHP.value() - currentHP.value();
		int regenHP = regenRate.value();

		if (healthNotFull(currentHP, maxHP)) {
			regenResult = regenHP > lackHP ? regenHP - lackHP : regenHP;
		}

		return PositiveInteger.of(regenResult);
	}

	private boolean healthNotFull(PositiveInteger currentHP, PositiveInteger maxHP) {
		return !currentHP.value().equals(maxHP.value());
	}

	private void setRegeneratedHP(PositiveInteger shieldRegenResult, PositiveInteger hullRegenResult) {
		if (hullRegenResult.value() != 0) {
			this.hullHP = PositiveInteger.of(this.hullHP.value() + hullRegenResult.value());
		}
		if (shieldRegenResult.value() != 0) {
			this.shieldHP = PositiveInteger.of(this.shieldHP.value() + shieldRegenResult.value());
		}
	}

}
