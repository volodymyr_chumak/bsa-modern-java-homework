package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.Subsystem;

public final class DockedShip implements ModularVessel {

	private final String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger powergridOutput;

	private PositiveInteger capacitorAmount;

	private final PositiveInteger capacitorRechargeRate;

	private final PositiveInteger speed;

	private final PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
	}

	public String getName() {
		return this.name;
	}

	public PositiveInteger getShieldHP() {
		return this.shieldHP;
	}

	public PositiveInteger getHullHP() {
		return this.hullHP;
	}

	public PositiveInteger getPowergridOutput() {
		return this.powergridOutput;
	}

	public PositiveInteger getCapacitorAmount() {
		return this.capacitorAmount;
	}

	public PositiveInteger getCapacitorRechargeRate() {
		return this.capacitorRechargeRate;
	}

	public PositiveInteger getSpeed() {
		return this.speed;
	}

	public PositiveInteger getSize() {
		return this.size;
	}

	public AttackSubsystem getAttackSubsystem() {
		return this.attackSubsystem;
	}

	public DefenciveSubsystem getDefenciveSubsystem() {
		return this.defenciveSubsystem;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {

		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem != null) {
			fitSubsystem(subsystem);
		}
		else {
			unfitAttackSubsystem();
		}
	}

	private void fitSubsystem(Subsystem subsystem) throws InsufficientPowergridException {
		int requiredPG = subsystem.getPowerGridConsumption().value();
		int difference = this.powergridOutput.value() - requiredPG;

		if (this.powergridOutput.value() >= requiredPG) {
			if (subsystem instanceof AttackSubsystem) {
				this.attackSubsystem = (AttackSubsystem) subsystem;
			}
			else if (subsystem instanceof DefenciveSubsystem) {
				this.defenciveSubsystem = (DefenciveSubsystem) subsystem;
			}
			this.powergridOutput = PositiveInteger.of(difference);
		}
		else {
			throw new InsufficientPowergridException(Math.abs(difference));
		}
	}

	private void unfitAttackSubsystem() {
		if (this.attackSubsystem != null) {
			this.powergridOutput = PositiveInteger
					.of(this.powergridOutput.value() + this.attackSubsystem.getPowerGridConsumption().value());
		}
		this.attackSubsystem = null;
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem != null) {
			fitSubsystem(subsystem);
		}
		else {
			unfitDefenciveSubsystem();
		}
	}

	private void unfitDefenciveSubsystem() {
		if (this.defenciveSubsystem != null) {
			this.powergridOutput = PositiveInteger
					.of(this.powergridOutput.value() + this.defenciveSubsystem.getPowerGridConsumption().value());
		}
		this.defenciveSubsystem = null;
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.attackSubsystem == null && this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		else if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		else if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}

		return new CombatReadyShip(DockedShip.this);
	}

}
